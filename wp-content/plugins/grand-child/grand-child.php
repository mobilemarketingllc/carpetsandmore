<?php 
/*
Plugin Name: Grandchild Theme
Plugin URI: http://www.wp-code.com/
Description: A WordPress Grandchild Theme (as a plugin)
Author: MM
Version: 1.0.90
Author URI: https://wpmaster.mm-dev.agency
*/
include( dirname( __FILE__ ) . '/include/apicaller.php' );
include( dirname( __FILE__ ) . '/include/constant.php' );
include( dirname( __FILE__ ) . '/include/helper.php' );
include( dirname( __FILE__ ) . '/include/track-leads.php' );    
/** GTM */
include( dirname( __FILE__ ) . '/include/gtm-manager.php' ); 

require_once plugin_dir_path( __FILE__ ) . 'example-plugin.php';
new Example_Background_Processing();

require_once( ABSPATH . "wp-includes/pluggable.php" );
    

 
     
global $jal_db_version;
$jal_db_version = '2.6';



function jal_install() {
	global $wpdb;
	global $jal_db_version;

	
	
	$charset_collate = $wpdb->get_charset_collate();

    $dataload = "SET FOREIGN_KEY_CHECKS=0";
    $wpdb->query( $dataload );

	$sql = "DROP TABLE IF EXISTS `wp_product_check`;
            CREATE TABLE `wp_product_check` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `skuid` varchar(100) COLLATE utf8_unicode_ci NOT NULL UNIQUE ,
            `post_id` bigint(20) unsigned NOT NULL UNIQUE,
            PRIMARY KEY (`id`),
            KEY `skuid` (`skuid`),
            KEY `fk_post_id` (`post_id`),
            CONSTRAINT `fk_post_id` FOREIGN KEY (`post_id`) REFERENCES `wp_posts` (`id`) ON DELETE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );


    
    $insert = "insert into wp_product_check(skuid,post_id) SELECT meta_value,post_id FROM wp_postmeta WHERE meta_key = 'sku'";
    $wpdb->query( $insert);
    $dataload = "SET FOREIGN_KEY_CHECKS=1;";
    $wpdb->query( $dataload );

    
    if(get_site_option( 'jal_db_version' ) !== false ){
        update_option( 'jal_db_version', $jal_db_version );
    }
    else{
        
        add_option( 'jal_db_version', $jal_db_version );
    }

	
}




function myplugin_update_db_check() {
    
    
    global $jal_db_version;
    if(get_site_option( 'jal_db_version' ) !== false ){
        if ( get_site_option( 'jal_db_version' ) != $jal_db_version ) {
            jal_install();
        }
    }
    else{
        
        jal_install();
    }
    
}

add_action( 'plugins_loaded', 'myplugin_update_db_check' );


        

if(isset($_POST['layoutopotion'])){
    
    (get_option('layoutopotion') !== null)?update_option('layoutopotion',$_POST['layoutopotion']):add_option('layoutopotion',$_POST['layoutopotion']);
    
}
if(isset($_POST['siteid']) && $_POST['siteid'] !="" && isset($_POST['clientcode']) && $_POST['clientcode'] !=""  && isset($_POST['clientsecret']) && $_POST['clientsecret'] !=""  )
    {

            //CALL Authentication API:
            $apiObj = new APICaller;
            $inputs = array('grant_type'=>'client_credentials','client_id'=>$_POST['clientcode'],'client_secret'=>$_POST['clientsecret']);
            $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
            
            
            if(isset($result['error'])){
                $msg =$result['error'];                
                $_SESSION['error'] = $msg;
                $_SESSION["error_desc"] =$result['error_description'];
                
            }
            else if(isset($result['access_token'])){

                get_option('CLIENT_CODE')?update_option('CLIENT_CODE',$_POST['clientcode']):add_option('CLIENT_CODE',$_POST['clientcode']);
                get_option('SITE_CODE')?update_option('SITE_CODE',$_POST['siteid']):add_option('SITE_CODE',$_POST['siteid']);
                get_option('CLIENTSECRET')?update_option('CLIENTSECRET',$_POST['clientsecret']):add_option('CLIENTSECRET',$_POST['clientsecret']);
                get_option('ACCESS_TOKEN')?update_option('ACCESS_TOKEN',$result['access_token']):add_option('ACCESS_TOKEN',$result['access_token']);
                get_option('CDE_ENV')?update_option('CDE_ENV',$_POST['instance-select']):add_option('CDE_ENV',$_POST['instance-select']); 
                get_option('CDE_LAST_SYNC_TIME')?update_option('CDE_LAST_SYNC_TIME',time()):add_option('CDE_LAST_SYNC_TIME',time()); 
                //API Call for Social Icon
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $sociallinks = $apiObj->call(BASEURL.get_option('SITE_CODE')."/".SOCIALURL,"GET",$inputs,$headers);
                
                if(isset($sociallinks['success']) && $sociallinks['success'] == 1 ){
                    $social_json =  json_encode($sociallinks['result']);
                    get_option('social_links') || get_option('social_links')==""?update_option('social_links',$social_json):add_option('social_links',$social_json);   
                }
                else{
                    $msg =$sociallinks['message'];                
                    $_SESSION['error'] = "Error SOCIAL LINKS";
                    $_SESSION["error_desc"] =$msg;
                    //echo $msg;
                    
                }
                
                //API Call for getting website INFO
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);
                
                
                if(isset($website['success']) && $website['success'] ){

                    $website_json = json_encode($website['result']);
                    get_option('website_json')?update_option('website_json',$website_json):add_option('website_json',$website_json);
                    if ( ! function_exists( 'post_exists' ) ) {
                        require_once( ABSPATH . 'wp-admin/includes/post.php' );
                    }
                    
                    
                    for($i=0;$i<count($website['result']['locations']);$i++){
                        $location_name = isset($website['result']['locations'][$i]['name'])?$website['result']['locations'][$i]['name']:"";

                        if(post_exists($location_name) == 0 && $location_name != "" ){

                            $array = array(
                                'post_title' => $location_name,
                                'post_type' => 'store-locations',
                                'post_content'  => "LOCATIONS",
                                'post_status'   => 'publish',
                                'post_author'   => 0,
                            );
                            $post_id = wp_insert_post( $array );
                            
                            
                        }
                    
                    }

                    if(isset($website['result']['sites'])){
                        for($k=0;$k<count($website['result']['sites']);$k++){
                            if($website['result']['sites'][$k]['instance'] == ENV){
                                update_option('blogname',$website['result']['sites'][$k]['name']);
                                $gtmid = $website['result']['sites'][$k]['gtmId'];
                                get_option( 'gtm_script_insert')?update_option( 'gtm_script_insert',$gtmid):update_option( 'gtm_script_insert',$gtmid);
                            }
                        }
                    }
                    //get_option( 'gtm_script_insert', )
                    
                }
                else{
                    $msg =$website['message'];                
                    //echo $msg;
                }
                //API Call for getting Contact INFO
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $contacts = $apiObj->call(BASEURL.CONTACTURL.get_option('SITE_CODE'),"GET",$inputs,$headers);
                
                if(isset($contacts['success']) && $contacts['success'] ){

                    $contacts_json = json_encode($contacts['result']);
                    get_option('contacts_json')?update_option('contacts_json',$contacts_json):add_option('contacts_json',$contacts_json);
                    
                    $phone = isset($contacts['result'][0]['phone'])?$contacts['result'][0]['phone']:'';
                    
                    get_option('api_contact_phone')?update_option('api_contact_phone',$phone):update_option('api_contact_phone',$phone);
                
                }
                else{
                    $msg =$contacts['message'];                
                    $_SESSION['error'] = "Error Contact Info";
                    $_SESSION["error_desc"] =$msg;
                }
                

                //API Call for geting product brand details:
                    
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $products = $apiObj->call(SOURCEURL.get_option('SITE_CODE')."/".PRODUCTURL,"GET",$inputs,$headers);
                
                
            
                if(isset($products) ){

                    $product_json = json_encode($products);
                    get_option('product_json')?update_option('product_json',$product_json):add_option('product_json',$product_json);
                }
                else{
                    $msg =$contacts['message'];                
                    $_SESSION['error'] = "Product Brand API";
                    $_SESSION["error_desc"] =$msg;
                }
            }

          
          
           
    }
    else{
        $msg = "Please fill all fields";
        //echo $msg;
    }
    
   
   
    
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    //'https://bitbucket.org/user-name/repo-name',
    'https://bitbucket.org/mobilemarketingllc/grandchild-plugin',
	__FILE__,
	'grand-child'
);

$myUpdateChecker->setAuthentication(array(
	'consumer_key' => 'Cn64bdU6RGrTTYpq4c',
	'consumer_secret' => 'fBxTEShRKubNn4WxrDDBymH4e4rGfqX6',
));



//Optional: Set the branch that contains the stable release.
if(ENV == 'dev' || ENV == 'staging')
    $myUpdateChecker->setBranch('dev');
else if(ENV == 'prod')
    $myUpdateChecker->setBranch('master');

function wpc_theme_global() {
    
    define( 'postpercol', '3' );
    include( dirname( __FILE__ ) . '/styles.php' );
    // define( 'productdetail_layout', 'box' );    
    define( 'productdetail_layout', 'box' );  
    wp_enqueue_style('lightbox-style', plugins_url('css/lightgallery.min.css', __FILE__));
    wp_enqueue_script('lightbox-js',plugins_url( 'js/lightgallery-all.min.js', __FILE__));
    wp_enqueue_script('script-js',plugins_url( 'js/script.js', __FILE__));
    gtm_head_script();
}
add_action('wp_head', 'wpc_theme_global');
 
function admin_style() {
    wp_enqueue_style('admin-styles', plugins_url('css/admin.css', __FILE__));
    wp_enqueue_script('script',plugins_url( '/js/retailer_v1.js', __FILE__ ));
}
add_action('admin_enqueue_scripts', 'admin_style');
// This filter replaces a complete file from the parent theme or child theme with your file (in this case the archive page).
// Whenever the archive is requested, it will use YOUR archive.php instead of that of the parent or child theme.
//add_filter ('archive_template', create_function ('', 'return plugin_dir_path(__FILE__)."archive.php";'));

function wpc_theme_add_headers () {
    wp_enqueue_style('font-awesome-styles', plugins_url('css/font-awesome.min.css', __FILE__));
    wp_enqueue_style('frontend-styles', plugins_url('css/styles.css', __FILE__));
}
// These two lines ensure that your CSS is loaded alongside the parent or child theme's CSS
add_action('init', 'wpc_theme_add_headers');
 
// In the rest of your plugin, add your normal actions and filters, just as you would in functions.php in a child theme.

function get_custom_post_type_template($single_template) {
    global $post;
      
    if ($post->post_type != 'post') {
         $single_template = dirname( __FILE__ ) . '/product-listing-templates/single-'.$post->post_type.'.php';
    }
    return $single_template;
}
add_filter( 'single_template', 'get_custom_post_type_template' );

//Code for adding Menu
//Nikhil Chinchane: 7 Jan 2019
//
function add_my_menu() {
    add_menu_page (
        'Retailer Settings', // page title 
        'Retailer Settings', // menu title
        'manage_options', // capability
        'my-menu-slug',  // menu-slug
        'my_menu_page',   // function that will render its output
        plugins_url('/img/theme-option-menu-icon.png', __FILE__)   // link to the icon that will be displayed in the sidebar
        //$position,    // position of the menu option
    );

    add_submenu_page('my-menu-slug', __('Retailer Product Data'), __('Retailer Product Data'), 'manage_options', 'retailer_product_data', 'retailer_product_data_html');
    add_submenu_page('my-menu-slug', __('Documentation'), __('Documentation'), 'manage_options', 'documentation', 'documentation_func');
}
add_action('admin_menu', 'add_my_menu');
function documentation_func(){
    ?>
        <div class="wrap" id="grandchild-backend">
            <h2>ShortCodes - Documentation </h2>
            <div class="description"></div>
                <table class="widefat striped" border="1" style="border-collapse:collapse;">
                    <thead>
                        <tr>
                            <th width="30%"><strong>Shortcode</strong></th>
                            <th width="35%"><strong>Example</strong></th>
                            <th width="30%"><strong>Description</strong></th>
                        </tr>
                        <tr>
                            <td><div class="copy">[shoparearug]</div></td>
                            <td>[shoparearug]</td>
                            <td>Display Area rugs six categories with area rug site link.</td>
                        </tr>
                        <tr>
                            <td>[area_rug_trading_products]</td>
                            <td>[area_rug_trading_products]</td>
                            <td>Display Area rugs trading products with area rug site link.</td>
                        </tr>
                        <tr>
                            <td>[storelocation_address "dir" "location name"] </td>
                            <td>[storelocation_address "dir" "HAMERNICK'S INTERIOR SOLUTIONS" ]</td>
                            <td>Display button with "Get Direction" text link to the store address</td>
                        </tr>
                        <tr>
                            <td>[storelocation_address "map" "location name"] </td>
                            <td>[storelocation_address "map" "HAMERNICK'S INTERIOR SOLUTIONS" ]</td>
                            <td>Display map of the store address</td>
                        </tr>
                        <tr>
                            <td>[storelocation_address "loc" "location name"] </td>
                            <td>[storelocation_address "loc" "HAMERNICK'S INTERIOR SOLUTIONS" ]</td>
                            <td>Display the store address with having link to the google map.</td>
                        </tr>
                        <tr>
                            <td>[storelocation_address "loc" "location name" "nolink"] </td>
                            <td>[storelocation_address "loc" "HAMERNICK'S INTERIOR SOLUTIONS" "nolink"]</td>
                            <td>Display the store address with having without link to the google map.</td>
                        </tr>
                        <tr>
                            <td>[storelocation_address "forwardingphone" "location name"] </td>
                            <td>[storelocation_address "forwardingphone" "HAMERNICK'S INTERIOR SOLUTIONS"]</td>
                            <td>Display the forwading number of the store with link.</td>
                        </tr>
                        <tr>
                            <td>[storelocation_address "forwardingphone" "location name" "nolink"] </td>
                            <td>[storelocation_address "forwardingphone" "HAMERNICK'S INTERIOR SOLUTIONS" "nolink"]</td>
                            <td>Display the forwading number of the store without link.</td>
                        </tr>
                        <tr>
                            <td>[getSocailIcons]</td>
                            <td>[getSocailIcons]</td>
                            <td>Display the social icons.</td>
                        </tr>
                        <tr>
                            <td>[Retailer "city"]</td>
                            <td>[storelocation_address "forwardingphone" "HAMERNICK'S INTERIOR SOLUTIONS" "nolink"]</td>
                            <td>Display the social icons.</td>
                        </tr>
                        <tr>
                            <td>[Retailer "state"]</td>
                            <td>[Retailer "state"]</td>
                            <td>Display the state of first store address which are added.</td>
                        </tr>
                        <tr>
                            <td>[Retailer "zipcode"]</td>
                            <td>[Retailer "zipcode"]</td>
                            <td>Display the zipcode of first store address which are added.</td>
                        </tr>
                        <tr>
                            <td>[Retailer "legalname"]</td>
                            <td>[Retailer "legalname"]</td>
                            <td>Display the legalname of site.</td>
                        </tr>
                        <tr>
                            <td>[Retailer "address"]</td>
                            <td>[Retailer "address"]</td>
                            <td>Display the address line of site.</td>
                        </tr>
                        <tr>
                            <td>[Retailer "phone" "nolink"]</td>
                            <td>[Retailer "phone"]</td>
                            <td>Display the phone without of site.</td>
                        </tr>
                        <tr>
                            <td>[Retailer "phone"]</td>
                            <td>[Retailer "phone"]</td>
                            <td>Display the phone of with link.</td>
                        </tr>
                        <tr>
                            <td>[Retailer "forwarding_phone"]</td>
                            <td>[Retailer "forwarding_phone"]</td>
                            <td>Display the forwarding phone with link of tel.</td>
                        </tr>
                        <tr>
                            <td>[Retailer "forwarding_phone" "nolink"]</td>
                            <td>[Retailer "forwarding_phone" "nolink"]</td>
                            <td>Display the Forwarding phone without the link .</td>
                        </tr>
                        <tr>
                            <td>[Retailer "companyname"]</td>
                            <td>[Retailer "companyname"]</td>
                            <td>Display the Company Name of site.</td>
                        </tr>
                        <tr>
                            <td>[Retailer "site_url"]</td>
                            <td>[Retailer "site_url"]</td>
                            <td>Display the Site URL of site.</td>
                        </tr>
                        <tr>
                            <td>[copyrights]</td>
                            <td>[copyrights]</td>
                            <td>Display the copyrights text use in footer.</td>
                        </tr>
                        
                        
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    <?php
}

//Create Form for client code and site id:
    function Calling_API_form($site,$clientcode) {
        if(isset($_SESSION['error'])){
     ?>
        <div class="notice notice-info error-info is-dismissible woo-info">
            <div class="info-image">
                <p> <img src='<?php echo plugins_url("/img/error.png", __FILE__)?>' width="48px"/></p>
            </div>
            <div class="info-descriptions">
                <div class="info-descriptions-title">
                        <h3><strong><?php echo ucfirst($_SESSION["error"]);?></strong></h3>
                </div>
                <p><?php echo $_SESSION["error_desc"];?></p>
                <p><b>Please Contact Plugin Development Team for further details</b></p>
            </div>
        </div>
        <?php
        }
        $form = '
        <div id="wpcontent1" class="client_info_wrap">
            <form name="pluginname" action="'.$_SERVER['REQUEST_URI'].'" method="POST">
                <table class="form-table">
                    <tr>
                        <th colspan="2"><h4>Enter Retailer Info</h4></th>
                    </tr>
                    '.( isset( $msg) ? '
                    <tr>
                        <td colspan="2">
                        <span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> '.$msg.'</span>
                            '.( !isset( $_POST['siteid'] ) ? '<span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> Site ID cannot be blank</span>' : null ).'
                            '.( !isset( $_POST['clientcode'] ) ? '<span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> Client Code cannot be blank</span>' : null ).'
                        </td>
                    </tr>
                    ' : null ).'
                    <tr>
                        <td width="150px">
                            <label for="siteid">Select environment <strong>*</strong></label>
                        </td>';
                        $cde = (get_option('CDE_ENV')) && get_option('CDE_ENV')!="" ?get_option('CDE_ENV'):""; 
                        $lasttimesync = get_option('CDE_LAST_SYNC_TIME')?date("F j, Y, g:i a", get_option('CDE_LAST_SYNC_TIME')):"Not yet sync";
                        if($cde != ""){
                            $form .='<td class="form-group">
                            <select name="instance-select">
                                <option value="prod" '.(  $cde=="prod" ? 'selected=selected' : null ).'>Production</option>
                                <option value="staging" '.(  $cde=="staging" ? 'selected=selected' : null ).'>Staging</option>
                                <option value="dev"  '.(  $cde=="dev" ? 'selected=selected' : null ).'>Development</option>
                            </select>
                        </td></tr>';
                        }
                        else{
                            $form .= '<td class="form-group">
                            <select name="instance-select" required>
                                <option value="">Choose Environment</option>
                                <option value="prod">Production</option>
                                <option value="staging">Staging</option>
                                <option value="dev">Development</option>
                            </select>
                        </td></tr>';
                        }
                        
                    
         $form .= '<tr>
                        <td width="150px">
                            <label for="siteid">Site Id <strong>*</strong></label>
                        </td>
                        <td class="form-group">
                            <input type="text" name="siteid" value="' . ( (get_option('SITE_CODE') ) ? get_option('SITE_CODE') : null ) . '" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="clientcode">Client Code <strong>*</strong></label>
                        </td>
                        <td class="form-group">
                            <input type="text" name="clientcode" value="' . ( get_option('CLIENT_CODE') ? get_option('CLIENT_CODE') : null ) . '" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="clientsecret">Client Secret <strong>*</strong></label>
                        </td>
                        <td class="form-group">
                            <input type="text" name="clientsecret" value="' . ( get_option('CLIENTSECRET') ? get_option('CLIENTSECRET') : null ) . '" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="form-group">
                            <button id="syncdata" type="submit" class="button button-primary" >Sync</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:right">Last sync time : '.$lasttimesync.'</td>
                    </tr>
                    
                </table>    
            </form>

            <form name="pluginname" action="'.$_SERVER['REQUEST_URI'].'" method="POST">
            <table class="form-table">
                <tr>
                    <th colspan="2"><h4>Settings</h4></th>
                </tr>
                <tr>
                    <td width="150px">
                        <label for="clientsecret">Product Detail Layout <strong>*</strong></label>
                    </td>
                    <td class="form-group">
                        <select name="layoutopotion" >
                            <option value="0" '.(  get_option('layoutopotion')==0 ? 'selected=selected' : null ).'>Default</option>
                            <option value="1" '.(  get_option('layoutopotion')==1 ? 'selected=selected' : null ).'>1</option>
                            <option value="2" '.(  get_option('layoutopotion')==2 ? 'selected=selected' : null ).'>2</option>
                            <option value="3" '.(  get_option('layoutopotion')==3 ? 'selected=selected' : null ).'>3</option>
                            <option value="4" '.(  get_option('layoutopotion')==4 ? 'selected=selected' : null ).'>4</option>
                            <option value="5" '.(  get_option('layoutopotion')==5 ? 'selected=selected' : null ).'>5</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="form-group">
                        <button id="syncdata" type="submit" class="button button-primary" >Save</button>
                    </td>
                </tr>
                
                
            </table>    
        </form>
        </div>
        ';
        echo $form;
        unset($_SESSION["error"]);
        unset($_SESSION["error_desc"]);
        
        
    }

    


function getRetailerInformation(){
    return '
		<div class="fl-button-wrap fl-button-width-auto fl-button-left">
			'.get_option('retailer_details').'
                    
</div>
	';
}
add_shortcode( 'getRetailerInformation', 'getRetailerInformation' );

function my_menu_page() {
        ?>
        <?php  
        if( isset( $_GET[ 'tab' ] ) ) {  
            $active_tab = $_GET[ 'tab' ];  
        } else {
            $active_tab = 'tab_one';
        }
        ?>  
        <div class="wrap" id="grandchild-backend">
            <h2>Retailer Settings</h2>
            <div class="description"></div>

            <?php Calling_API_form('','');?>
            <?php settings_errors(); ?> 

            <?php
                //$details = json_decode(get_option('retailer_details'));
                $website_json =  json_decode(get_option('website_json'));
                $details = json_decode(get_option('social_links'));
                if($website_json || $details) { ?>
                <table class="widefat striped" border="1" style="border-collapse:collapse;">
                    <thead>
                        <tr>
                            <th width="25%"><strong>Name</strong></th>
                            <th width="75%"><strong>Values</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                <?php
                   
                    foreach($website_json as $key => $value){   
                        if(!is_array($value)){
                            echo "<tr><td>".ucfirst($key)."</td><td>".ucfirst($value)."</td></tr>";
                        }   
                        
                    }
                    echo "<tr><th colspan='2'><strong>Social Platforms</strong></th></tr>";
                    foreach($details as $key => $value){   
                        echo "<tr><td>".ucfirst($value->platform)."</td><td>".$value->url."</td></tr>";
                    }
                    echo "<tr><th colspan='2'><strong>Site information (".ENV.")</strong></th></tr>";
                    $website =  $website_json;
                    for($i=0;$i<count($website->sites);$i++){
                        if($website->sites[$i]->instance == ENV){
                            foreach($website->sites[$i] as $key => $value){   
                                if($key)
                                echo "<tr><td>".ucfirst($key)."</td><td>".($value)."</td></tr>";
                            }
                        }
                   
                }
                    echo "<tr><th colspan='2'><strong>Locations</strong></th></tr>";
                    $website =  $website_json;
                    for($i=0;$i<count($website->locations);$i++){

                        $location_name = isset($website_json->locations[$i]->name)?$website->locations[$i]->name:"";
                
                        //$website->locations[$i]->address.", ".$website->locations[$i]->city.", ".$website->locations[$i]->state.", ".$website->locations[$i]->postalCode;
                        $location_address  = isset($website->locations[$i]->address)?$website->locations[$i]->address:"";
                        $location_address .= isset($website->locations[$i]->city)?" , ".$website->locations[$i]->city:"";
                        $location_address .= isset($website->locations[$i]->state)?" , ".$website->locations[$i]->state:"";
                        $location_address .= isset($website->locations[$i]->postalCode)?" , ".$website->locations[$i]->postalCode:"";
                        
                        
                
                        $location_phone = isset($website->locations[$i]->phone)?$website->locations[$i]->phone:"";
                        echo "<tr><td>Name</td><td><b>".$location_name."</b></td></tr>";
                        echo "<tr><td>Address</td><td>".$location_address."</td></tr>";
                        echo "<tr><td>Phone</td><td>".$website->locations[$i]->phone."</td></tr>";
                        echo "<tr><td>Forwarding phone</td><td>".$website->locations[$i]->forwardingPhone."</td></tr>";
                        echo "<tr><td>License Number</td><td>".$website->locations[$i]->licenseNumber."</td></tr>";
                        $weekdays = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
                        $openinghrs = "<ul style='display:block;'>";
                        for ($j = 0; $j < count($weekdays); $j++) {
                            $location .= $website->locations[$i]->monday;
                            if (isset($website->locations[$i]->{$weekdays[$j]})) {
                                $openinghrs .= '<li>'.ucfirst($weekdays[$j]).' : <span>'.$website->locations[$i]->{$weekdays[$j]}.'</span></li>';
                            }
                        }
                        $openinghrs .= "</ul>";
                        echo "<tr><td>Opening Hrs</td><td>".$openinghrs."</td></tr>";
                    }
                    $contacts = json_decode(get_option('website_json'));
                    echo "<tr><th colspan='2'><strong>Contacts</strong></th></tr>";

                    if(is_array($contacts->contacts)){
                        
                        for($j=0;$j<count($contacts->contacts);$j++){
                            foreach($contacts->contacts[$j] as $key => $value){   
                                echo "<tr><td>".ucfirst($key)."</td><td>".$value."</td></tr>";
                            }
                        }
                    }
                    
                  //  echo do_shortcode("[storelocation_address alldata]"); 
                ?>
                    </tbody>
                </table>
                <?php 
                }
                    
            ?>

            <h2 class="nav-tab-wrapper" style="display:none;" >  
                <a href="?page=my-menu-slug&tab=tab_one" class="nav-tab <?php echo $active_tab == 'tab_one' ? 'nav-tab-active' : ''; ?>">API Details</a>  
                <a href="?page=my-menu-slug&tab=tab_two" class="nav-tab <?php echo $active_tab == 'tab_two' ? 'nav-tab-active' : ''; ?>">Contact Info</a>  
                <a href="?page=my-menu-slug&tab=tab_four" class="nav-tab <?php echo $active_tab == 'tab_four' ? 'nav-tab-active' : ''; ?>">Social Media</a>  
                <a href="?page=my-menu-slug&tab=tab_three" class="nav-tab <?php echo $active_tab == 'tab_three' ? 'nav-tab-active' : ''; ?>">Miscellaneous</a>  
            </h2>  

            
            <form method="post" action="options.php" style="display:none;"  > 
            <?php
                if( $active_tab == 'tab_one' ) {  

                    settings_fields( 'setting-group-1' );
                    do_settings_sections( 'my-menu-slug' );
                    submit_button();

                } elseif( $active_tab == 'tab_two' )  {

                    settings_fields( 'setting-group-2' );
                    do_settings_sections( 'my-menu-slug-1' );
                    submit_button();

                }
                elseif( $active_tab == 'tab_three' )  {

                    settings_fields( 'setting-group-3' );
                    do_settings_sections( 'my-menu-slug-3' );
                    submit_button();

                }
                elseif( $active_tab == 'tab_four' )  {

                    settings_fields( 'setting-group-4' );
                    do_settings_sections( 'my-menu-slug-4' );
                    submit_button();

                }
                

               
            ?>

                <?php //submit_button(); ?> 
            </form> 

        </div>
        <?php
}

/* ----------------------------------------------------------------------------- */
/* Setting Sections And Fields */
/* ----------------------------------------------------------------------------- */ 

function sandbox_initialize_theme_options() {  
    
    add_settings_section(  
        'page_1_section',         // ID used to identify this section and with which to register options  
        '',                  // Title to be displayed on the administration page  
        'page_1_section_callback', // Callback used to render the description of the section  
        'my-menu-slug'                           // Page on which to add this section of options  

    );

    add_settings_section(  
        'page_2_section',         // ID used to identify this section and with which to register options  
        '',                  // Title to be displayed on the administration page  
        'page_2_section_callback', // Callback used to render the description of the section  
        'my-menu-slug-1'                           // Page on which to add this section of options  
    );
    add_settings_section(  
        'page_3_section',         // ID used to identify this section and with which to register options  
        '',                  // Title to be displayed on the administration page  
        'page_3_section_callback', // Callback used to render the description of the section  
        'my-menu-slug-3'                           // Page on which to add this section of options  
    );
    
    add_settings_section(  
        'page_4_section',         // ID used to identify this section and with which to register options  
        '',                  // Title to be displayed on the administration page  
        'page_4_section_callback', // Callback used to render the description of the section  
        'my-menu-slug-4'                           // Page on which to add this section of options  
    );
	
    /* ----------------------------------------------------------------------------- */
    /* Option 1 */
    /* ----------------------------------------------------------------------------- */ 

   /*  add_settings_field (   
        'option_1',                      // ID used to identify the field throughout the theme  
        'Option 1',                           // The label to the left of the option interface element  
        'option_1_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug',                          // The page on which this option will be displayed  
        'page_1_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'This is the description of the option 1',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-1',  
        'option_1'  
    ); */

    //Social Media Tab

    
    
	 add_settings_field (   
        'api_fb_link',                      // ID used to identify the field throughout the theme  
        'Facebook',                           // The label to the left of the option interface element  
        'fb_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-4',                          // The page on which this option will be displayed  
        'page_4_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter FB URL',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-4',  
        'api_fb_link'  
    );

    add_settings_field (   
        'api_youtube_link',                      // ID used to identify the field throughout the theme  
        'Youtube',                           // The label to the left of the option interface element  
        'youtube_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-4',                          // The page on which this option will be displayed  
        'page_4_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter Youtube URL',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-4',  
        'api_youtube_link'  
    );
    
    add_settings_field (   
        'api_twitter_link',                      // ID used to identify the field throughout the theme  
        'Twitter',                           // The label to the left of the option interface element  
        'twitter_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-4',                          // The page on which this option will be displayed  
        'page_4_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter Twitter URL',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-4',  
        'api_twitter_link'  
    );

    add_settings_field (   
        'api_gplus_link',                      // ID used to identify the field throughout the theme  
        'GPlus',                           // The label to the left of the option interface element  
        'gplus_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-4',                          // The page on which this option will be displayed  
        'page_4_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter GPlus URL',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-4',  
        'api_gplus_link'  
    );
    add_settings_field (   
        'api_pinterest_link',                      // ID used to identify the field throughout the theme  
        'Pinterest',                           // The label to the left of the option interface element  
        'pinterest_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-4',                          // The page on which this option will be displayed  
        'page_4_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter Pinterest URL',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-4',  
        'api_pinterest_link'  
    );
    add_settings_field (   
        'api_linkedin_link',                      // ID used to identify the field throughout the theme  
        'LinkedIn',                           // The label to the left of the option interface element  
        'linkedin_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-4',                          // The page on which this option will be displayed  
        'page_4_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter LinkedIn URL',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-4',  
        'api_linkedin_link'  
    );
    add_settings_field (   
        'api_insta_link',                      // ID used to identify the field throughout the theme  
        'Instagram',                           // The label to the left of the option interface element  
        'insta_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-4',                          // The page on which this option will be displayed  
        'page_4_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter Instagram URL',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-4',  
        'api_insta_link'  
    );
    add_settings_field (   
        'api_houzz_link',                      // ID used to identify the field throughout the theme  
        'Houzz',                           // The label to the left of the option interface element  
        'houzz_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-4',                          // The page on which this option will be displayed  
        'page_4_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter Houzz URL',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-4',  
        'api_houzz_link'  
    );
    //End of social media tab

	 add_settings_field (   
        'api_url_sandbox',                      // ID used to identify the field throughout the theme  
        'URL',                           // The label to the left of the option interface element  
        'url_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug',                          // The page on which this option will be displayed  
        'page_1_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter API Base URL',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-1',  
        'api_url_sandbox'  
    );


    add_settings_field (   
        'api_username_sandbox',                      // ID used to identify the field throughout the theme  
        'Username',                           // The label to the left of the option interface element  
        'username_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug',                          // The page on which this option will be displayed  
        'page_1_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter API Username',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-1',  
        'api_username_sandbox'  
    );

    add_settings_field (   
        'api_password_sandbox',                      // ID used to identify the field throughout the theme  
        'Password',                           // The label to the left of the option interface element  
        'password_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug',                          // The page on which this option will be displayed  
        'page_1_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter API Password',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-1',  
        'api_password_sandbox'  
    );

    /* add_settings_field (   
        'api_colorcode_sandbox',                      // ID used to identify the field throughout the theme  
        'ColorCode',                           // The label to the left of the option interface element  
        'colorcode_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'page_3_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter Color code',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-3',  
        'api_colorcode_sandbox'  
    );
 */
     add_settings_field (   
        'api_colorcode_sandbox',                      // ID used to identify the field throughout the theme  
        'Color Code',                           // The label to the left of the option interface element  
        'colorcode_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-3',                          // The page on which this option will be displayed  
        'page_3_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter Color code',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-3',  
        'api_colorcode_sandbox'  
    ); 

    add_settings_field (   
        'api_product_col_sandbox',                      // ID used to identify the field throughout the theme  
        'Product Listin Columns',                           // The label to the left of the option interface element  
        'product_col_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-3',                          // The page on which this option will be displayed  
        'page_3_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter Product Columns code',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-3',  
        'api_product_col_sandbox'  
    );
    add_settings_field (   
        'api_product_detailspage_sandbox',                      // ID used to identify the field throughout the theme  
        'Product Detail Layout',                           // The label to the left of the option interface element  
        'product_detail_page_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-3',                          // The page on which this option will be displayed  
        'page_3_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Select Product Details page layout',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-3',  
        'api_product_detailspage_sandbox'  
    );
    
    add_settings_field (   
        'api_contact_address_sandbox',                      // ID used to identify the field throughout the theme  
        'Contact Address',                           // The label to the left of the option interface element  
        'product_contact_address_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-1',                          // The page on which this option will be displayed  
        'page_2_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Contact Address',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-2',  
        'api_contact_address_sandbox'  
    );

    add_settings_field (   
        'api_contact_phone_sandbox',                      // ID used to identify the field throughout the theme  
        'Contact Phone',                           // The label to the left of the option interface element  
        'product_contact_phone_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-1',                          // The page on which this option will be displayed  
        'page_2_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Contact Phone',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-2',  
        'api_contact_phone_sandbox'  
    );

    add_settings_field (   
        'api_contact_url_sandbox',                      // ID used to identify the field throughout the theme  
        'Get Direction URL',                           // The label to the left of the option interface element  
        'product_contact_url_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-1',                          // The page on which this option will be displayed  
        'page_2_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Get Direction URL',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-2',  
        'api_contact_url_sandbox'  
    );

    add_settings_field (   
        'api_opening_hr_fields_sandbox',                      // ID used to identify the field throughout the theme  
        'Enter Opening Hours',                           // The label to the left of the option interface element  
        'product_opening_hr_fields_sandbox_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug-1',                          // The page on which this option will be displayed  
        'page_2_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter Opening Hours',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-2',  
        'api_opening_hr_fields_sandbox'  
    );
	
	/// Production Values

    add_settings_field (   
        'api_url_live',                      // ID used to identify the field throughout the theme  
        'URL (Production)',                           // The label to the left of the option interface element  
        'url_live_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug',                          // The page on which this option will be displayed  
        'page_1_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter API Base URL',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
        'setting-group-1',  
        'api_url_live'  
    );

    add_settings_field (   
        'api_username_live',                      // ID used to identify the field throughout the theme  
        'Username (Production)',                           // The label to the left of the option interface element  
        'username_live_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug',                          // The page on which this option will be displayed  
        'page_1_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter API Username',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
       'setting-group-1',    
        'api_username_live'  
    );

    add_settings_field (   
        'api_password_live',                      // ID used to identify the field throughout the theme  
        'Password (Production)',                           // The label to the left of the option interface element  
        'password_live_callback',   // The name of the function responsible for rendering the option interface  
        'my-menu-slug',                          // The page on which this option will be displayed  
        'page_1_section',         // The name of the section to which this field belongs  
        array(                              // The array of arguments to pass to the callback. In this case, just a description.  
            'Enter API Password',
        )  
    );  
    register_setting(  
        //~ 'my-menu-slug',  
       'setting-group-1',                           // The page on which this option will be displayed  
        'api_password_live'  
    );
    //End Production Values

	
    /* ----------------------------------------------------------------------------- */
    /* Option 2 */
    /* ----------------------------------------------------------------------------- */     

    add_settings_field (   
        'option_2',  // ID -- ID used to identify the field throughout the theme  
        'Option 2', // LABEL -- The label to the left of the option interface element  
        'option_2_callback', // CALLBACK FUNCTION -- The name of the function responsible for rendering the option interface  
        'my-menu-slug', // MENU PAGE SLUG -- The page on which this option will be displayed  
        'page_2_section', // SECTION ID -- The name of the section to which this field belongs  
        array( // The array of arguments to pass to the callback. In this case, just a description.  
            'This is the description of the option 2', // DESCRIPTION -- The description of the field.
        )  
    );
    register_setting(  
        'setting-group-2',  
        'option_2'  
    );

} // function sandbox_initialize_theme_options
add_action('admin_init', 'sandbox_initialize_theme_options');

function page_0_section_callback(){
    echo '<div style="margin:20px 0 25px 0;">
            <div class="form-group">
                <label for="siteid">Site Id:</label>
                <input type="text" class="form-control" id="siteid" placeholder="Site Id">
            </div>
            <div class="form-group">
                <label for="clientcode">Client Code:</label>
                <input type="text" class="form-control" id="clientcode" placeholder="Client COde">
            </div>
 <button type="button" class="btn btn-default">Submit</button>
</div>';
}
function page_1_section_callback() {  
    echo '';  
} // function page_1_section_callback
function page_2_section_callback() {  
    //echo '<p>Section Description here</p>';  
} // function page_1_section_callback

function page_3_section_callback() {  
    //echo '<p>Section Description here</p>';  
} // function page_1_section_callback

function page_4_section_callback(){}
/* ----------------------------------------------------------------------------- */
/* Field Callbacks */
/* ----------------------------------------------------------------------------- */ 

function houzz_sandbox_callback($args){
    ?>
    <input type="text" id="api_houzz_link" class="api_houzz_link api-textbox" name="api_houzz_link" value="<?php echo get_option('api_houzz_link') ?>">
    <p class="description api_houzz_link"> <?php echo $args[0] ?> </p>
    <?php  
}
function insta_sandbox_callback($args){
    ?>
    <input type="text" id="api_insta_link" class="api_insta_link api-textbox" name="api_insta_link" value="<?php echo get_option('api_insta_link') ?>">
    <p class="description api_insta_link"> <?php echo $args[0] ?> </p>
    <?php  
}

function pinterest_sandbox_callback($args){
    ?>
    <input type="text" id="api_pinterest_link" class="api_pinterest_link api-textbox" name="api_pinterest_link" value="<?php echo get_option('api_pinterest_link') ?>">
    <p class="description api_pinterest_link"> <?php echo $args[0] ?> </p>
    <?php  
}

function linkedin_sandbox_callback($args){
    ?>
    <input type="text" id="api_linkedin_link" class="api_linkedin_link api-textbox" name="api_linkedin_link" value="<?php echo get_option('api_linkedin_link') ?>">
    <p class="description api_linkedin_link"> <?php echo $args[0] ?> </p>
    <?php  
}

function gplus_sandbox_callback($args){
    ?>
    <input type="text" id="api_gplus_link" class="api_gplus_link api-textbox" name="api_gplus_link" value="<?php echo get_option('api_gplus_link') ?>">
    <p class="description api_gplus_link"> <?php echo $args[0] ?> </p>
    <?php  
}
function twitter_sandbox_callback($args){
    ?>
    <input type="text" id="api_twitter_link" class="api_twitter_link api-textbox" name="api_twitter_link" value="<?php echo get_option('api_twitter_link') ?>">
    <p class="description api_twitter_link"> <?php echo $args[0] ?> </p>
    <?php  
}
function youtube_sandbox_callback($args){
    ?>
    <input type="text" id="api_youtube_link" class="api_youtube_link api-textbox" name="api_youtube_link" value="<?php echo get_option('api_youtube_link') ?>">
    <p class="description api_youtube_link"> <?php echo $args[0] ?> </p>
    <?php  
}
function fb_sandbox_callback($args){
    ?>
    <input type="text" id="api_fb_link" class="api_fb_link api-textbox" name="api_fb_link" value="<?php echo get_option('api_fb_link') ?>">
    <p class="description api_fb_link"> <?php echo $args[0] ?> </p>
    <?php  
}
function product_opening_hr_fields_sandbox_callback($args){
    ?>
    <textarea width="300px" type="text" id="api_opening_hr_fields_sandbox" class="api_opening_hr_fields_sandbox" name="api_opening_hr_fields_sandbox" ><?php echo get_option('api_opening_hr_fields_sandbox') ?></textarea>
    <p class="description api_opening_hr_fields_sandbox"> <?php echo $args[0] ?> </p>
    <?php  
}
function option_1_callback($args) {  
    ?>
    <input type="text" id="option_1" class="option_1 api-textbox" name="option_1" value="<?php echo get_option('option_1') ?>">
    <p class="description option_1"> <?php echo $args[0] ?> </p>
    <?php      
}   
function url_sandbox_callback($args){
	 ?>
    <input type="text" id="api_url_sandbox" class="api_url_sandbox api-textbox" name="api_url_sandbox" value="<?php echo get_option('api_url_sandbox') ?>">
    <p class="description api_url_sandbox"> <?php echo $args[0] ?> </p>
    <?php   
}
function username_sandbox_callback($args){
    ?>
   <input type="text" id="api_username_sandbox" class="api_username_sandbox api-textbox" name="api_username_sandbox" value="<?php echo get_option('api_username_sandbox') ?>">
   <p class="description api_username_sandbox"> <?php echo $args[0] ?> </p>
   <?php   
}
function password_sandbox_callback($args){
    ?>
   <input type="text" id="api_password_sandbox" class="api_password_sandbox api-textbox" name="api_password_sandbox" value="<?php echo get_option('api_password_sandbox') ?>">
   <p class="description api_password_sandbox"> <?php echo $args[0] ?> </p>
   <?php   
}
function colorcode_sandbox_callback($args){
    ?>
   <input type="text" id="api_colorcode_sandbox" class="api_colorcode_sandbox api-textbox" name="api_colorcode_sandbox" value="<?php echo get_option('api_colorcode_sandbox') ?>">
   <p class="description api_colorcode_sandbox"> <?php echo $args[0] ?> </p>
   <?php   
}
function product_col_sandbox_callback($args){
    ?>
   <select type="text" id="api_product_col_sandbox" class="api_product_col_sandbox api-selectbox" name="api_product_col_sandbox" >
     <option value="2" <?php echo get_option('api_product_col_sandbox')== 2?"selected":""?>>2</option>
     <option value="3" <?php echo get_option('api_product_col_sandbox')== 3?"selected":""?>>3</option>
     <option value="4" <?php echo get_option('api_product_col_sandbox')== 4?"selected":""?>>4</option>
     <option value="5" <?php echo get_option('api_product_col_sandbox')== 5?"selected":""?>>5</option>

   </select>
   <p class="description api_product_col_sandbox"> <?php echo $args[0] ?> </p>
   <?php   
}
function product_detail_page_sandbox_callback($args){
    ?>
   <select type="text" id="api_product_detailspage_sandbox" class="api_product_detailspage_sandbox api-selectbox" name="api_product_detailspage_sandbox" >
   <option value="" >-- </option>
     <option value="full_width" <?php echo get_option('api_product_detailspage_sandbox')== "full_width"?"selected":""?>>Full width</option>
     <option value="fixed_width" <?php echo get_option('api_product_detailspage_sandbox')== "fixed_width"?"selected":""?>>Fix width</option>
   </select>
   <p class="description api_url_sandbox"> <?php echo $args[0] ?> </p>
   <?php   
}

function product_contact_address_sandbox_callback($args){
    ?>
    <input type="text" id="api_contact_address_sandbox" class="contact_address api-textbox" name="api_contact_address_sandbox" value="<?php echo get_option('api_contact_address_sandbox') ?>">
    <p class="description api_contact_address_sandbox"> <?php echo $args[0] ?> </p>
    <?php   
}

function product_contact_phone_sandbox_callback($args){
    ?>
    <input type="text" id="api_contact_phone_sandbox" class="contact_address api-textbox" name="api_contact_phone_sandbox" value="<?php echo get_option('api_contact_phone_sandbox') ?>">
    <p class="description api_contact_phone_sandbox"> <?php echo $args[0] ?> </p>
    <?php   
}

function product_contact_url_sandbox_callback($args){
    ?>
    <input type="text" id="api_contact_url_sandbox" class="contact_address api-textbox" name="api_contact_url_sandbox" value="<?php echo get_option('api_contact_url_sandbox') ?>">
    <p class="description api_contact_url_sandbox"> <?php echo $args[0] ?> </p>
    <?php 
}
// end sandbox_toggle_header_callback

function url_live_callback($args){
    ?>
   <input type="text" id="api_url_live" class="api_url_live api-textbox" name="api_url_live" value="<?php echo get_option('api_url_live') ?>">
   <p class="description api_url_live"> <?php echo $args[0] ?> </p>
   <?php   
}
function username_live_callback($args){
   ?>
  <input type="text" id="api_username_live" class="api_username_live api-textbox" name="api_username_live" value="<?php echo get_option('api_username_live') ?>">
  <p class="description api_username_live"> <?php echo $args[0] ?> </p>
  <?php   
}
function password_live_callback($args){
   ?>
  <input type="text" id="api_password_live" class="api_password_live api-textbox" name="api_password_live" value="<?php echo get_option('api_password_live') ?>">
  <p class="description api_password_live"> <?php echo $args[0] ?> </p>
  <?php   
}
function option_2_callback($args) {  
    ?>
    <textarea id="option_2" class="option_2" name="option_2" rows="5" cols="50"><?php echo get_option('option_2') ?></textarea>
    <p class="description option_2"> <?php echo $args[0] ?> </p>
    <?php      
} 


function retailer_product_data_html(){
    
    ?>
    <div id="wpcontent1" class="client_info_wrap">
  
       <form name="productfrm" action="/wp-admin/admin.php?page=retailer_product_data" method="POST">
           <table class="form-table">
               <tbody>
                   <tr>
                       <th colspan="4">
                           <h4>Retailer Flooring Data</h4>
                       </th>
                   </tr>
                   <tr>
                       <td width="300px">
                           <label for="maincat">Select Flooring Category(Main) <strong>*</strong></label>
                       </td>
                    </tr>
                                <tr>
                                    <th class="form-group">Product Type</th>
                                    <th class="form-group">Brand</th>
                                    <th class="form-group">Deal Brand</th>
                                    <th class="form-group">Action</th>
                                </tr>

                       <?php
                            $product_json =  json_decode(get_option('product_json'));                       
                            $brandmapping = array(
                                "carpet"=>"carpeting",
                                "hardwood"=>"hardwood_catalog",
                                "laminate"=>"laminate_catalog",
                                "lvt"=>"luxury_vinyl_tile",
                                "tile"=>"tile_catalog",
                                "waterproof"=>"solid_wpc_waterproof"
                            );
                            for($i=0;$i < count($product_json);$i++){
                                ?>
                                <tr>
                                    <form name="productfrm" action="/wp-admin/admin.php?page=retailer_product_data" method="POST">
                                        <td class="form-group"><?php echo ucfirst($product_json[$i]->productType); ?></td>
                                        <?php
                                        
                                            if( strcmp($product_json[$i]->productType,"Carpet")){
                                                ?>
                                                <input type="hidden" name="api_product_data_category" value="<?php echo $brandmapping[$product_json[$i]->productType]?>" />
                                                <input type="hidden" name="api_product_data_brand" value="<?php echo $product_json[$i]->manufacturer;?>" />
                                                <?php
                                            }
                                        ?>
                                        <td class="form-group"><?php echo $product_json[$i]->brand. " ".$product_json[$i]->manufacturer;; ?></td>
                                        <td class="form-group"><?php echo $product_json[$i]->dealerBrand; ?></td>
                                        <td class="form-group"><button id="syncdata-pro" type="submit" class="button button-primary">Sync</button></td>
                                    </form>
                                </tr>


                                <?php
                            }

                        ?>
                       <td class="form-group">
                          <!--  <select id="api_product_data_category" class="api_product_data_category api-selectbox" name="api_product_data_category">
                           <option value="carpeting">Carpet</option>
                           <option value="hardwood_catalog">Hardwood</option>
                           <option value="laminate_catalog">Laminate</option>
                           <option value="luxury_vinyl_tile">Luxury Vinyl</option>
                           <option value="tile_catalog">Tile</option>
                           <option value="solid_wpc_waterproof">Waterproof</option>
                         </select> -->
                       </td>
                   </tr>
                   <tr>
                      
                       <td class="form-group">
                           <!-- <select id="api_product_data_brand" class="api_product_data_brand api-selectbox" name="api_product_data_brand">
                           <option value="dreamweaver">Dream Weaver</option>
                           <option value="mohawk">Mohawk</option>
                           <option value="armstrong">Armstrong</option>
                           <option value="mannington">Mannington</option>
                           <option value="coretec">COREtec</option>
                           <option value="daltile">Daltile</option>     
                         </select> -->
                       </td>
                   </tr>
                   <tr>
                       <td></td>
                       <td class="form-group">
                          <!--  <button id="syncdata-pro" type="submit" class="button button-primary">Sync Product Data</button> -->
                       </td>
                   </tr>
               </tbody>
           </table>
       </form>
   </div>
       <?php
       if(isset($_POST['api_product_data_category'] ) && isset($_POST['api_product_data_brand'] )){
                ob_start();
                $upload = wp_upload_dir();
                $upload_dir = $upload['basedir'];
                $upload_dir = $upload_dir . '/sfn-data';
                if (! is_dir($upload_dir)) {
                    mkdir( $upload_dir, 0700 );
                } 

              /*   if($_POST['api_product_data_category']=='carpeting'){
                    $sfnapi_category = 'carpet';
                }
                elseif($_POST['api_product_data_category']=='hardwood_catalog') {
                    $sfnapi_category = 'hardwood';
                }
                elseif($_POST['api_product_data_category']=='laminate_catalog') {
                    $sfnapi_category = 'laminate';
                }
                elseif($_POST['api_product_data_category']=='luxury_vinyl_tile') {
                    $sfnapi_category = 'lvt';
                }
                elseif($_POST['api_product_data_category']=='tile_catalog') {
                    $sfnapi_category = 'tile';
                }
                elseif($_POST['api_product_data_category']=='solid_wpc_waterproof') {
                    $sfnapi_category = 'waterproof';
                } */
                //$_POST['api_product_data_category']
                //echo $brandmapping[$_POST['api_product_data_category']];exit;
                $sfnapi_category_array = array_keys($brandmapping,$_POST['api_product_data_category']);
                $sfnapi_category = $sfnapi_category_array[0];
                
                $permfile = $upload_dir.'/'.$_POST['api_product_data_category'].'_'.$_POST['api_product_data_brand'].'.csv';
               //$res = 'https://sfn.mm-api.agency/jbrcttlt/www/'.$sfnapi_category.'/'.$_POST['api_product_data_brand'].'.csv?status=active&status=pending&status=dropped&status=gone';
               $res = SOURCEURL.get_option('SITE_CODE').'/www/'.$sfnapi_category.'/'.$_POST['api_product_data_brand'].'.csv?status=active&status=pending&status=dropped&status=gone';

               $tmpfile = download_url( $res, $timeout = 300 );
//               var_dump($tmpfile);
                    if(is_file($tmpfile)){
                        copy( $tmpfile, $permfile );
                        unlink( $tmpfile ); 
                        require_once plugin_dir_path( __FILE__ ) . 'example-plugin.php';            
                        $data_url =   admin_url( '/admin.php?page=retailer_product_data&process=all&main_category='.$_POST['api_product_data_category'].'&product_brand='.$_POST['api_product_data_brand'].'');
//unset($_SESSION['error']);
                     }
                     else{
                        $_SESSION['error'] = "Couldn't find products for this category";
                        
                    }
               

              
          ?>
          <script>
          window.location.href = "<?php echo $data_url; ?>";
          </script>
          <?php exit();}
}
   
