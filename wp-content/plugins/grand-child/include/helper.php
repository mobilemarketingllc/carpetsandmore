<?php

//Tranding Product ShortCode

function shopofarearug($arg)
{
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    $data = array(
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-colors/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/Color.png', __FILE__),
            'title'=> "Shop by Color"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-styles/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/style.png', __FILE__),
            'title'=> "Shop by Style"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-sizes/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/Size.png', __FILE__),
            'title'=> "Shop by Size"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-patterns/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/patterns.png', __FILE__),
            'title'=> "Shop by Patterns"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-brands/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/brands.png', __FILE__),
            'title'=> "Shop by Brands"
        ),
        array(
            'url'=>'https://rugs.shop/on-sale/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/sale.png', __FILE__),
            'title'=> "Area Rug Sale"
        ),
       
    );
    $result ="";
    $result.= '<div class="products-list column-3"><ul class="product-list">';
    for ($i=0;$i<count($data);$i++) {
        $result .='<li class="product-li-three-row">
                    <div class="product-inner">
                        <div class="product-img-holder">
                            <a href="'.$data[$i]['url'].'" target="_blank" itemprop="url" class="">
                                <img src="'.$data[$i]['image_url'].'" alt="'.$data[$i]['title'].'" />
                            </a>
                        </div>
                        <h3 class="fl-callout-title"><a href="'.$data[$i]['url'].'" target="_blank">'.$data[$i]['title'].'</a></h3>
                    </div>
                </li>';
    }
    $result.= '</ul></div>';
    return $result;
}

add_shortcode('shoparearug', 'shopofarearug');

//Tranding Product ShortCode

function arearugProductList($arg)
{
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    
    $data = array(
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'99.00',
            'title'=> "PETRA MULTI",
            'collection' => 'Spice Market',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'499.00',
            'title'=> "Esperance Seaglass",
            'collection' => 'Titanium Collection',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'156.00',
            'title'=> "Nirvana Indigo",
            'collection' => 'Cosmopolitan',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mobilem.liquifire.com/mobilem?source=url[http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg]&scale=size[280x415]&sink',
            'price'=>'62.40',
            'title'=> "7139a",
            'collection' => 'Andora',
            'brand' => 'Oriental Weavers'
        )
    );
    $result ="";
    $result.= '<div class="products-list"><ul class="product-list">';
    for ($i=0;$i<count($data);$i++) {
        $result .='<li class="product-li">
            <div class="product-inner">
                <div class="product-img-holder">
                    <a href="'.$data[$i]['url'].'" target="_blank"><img src="'.$data[$i]['image_url'].'" alt="'.$data[$i]['title'].'" /></a>
                </div>
                <div class="product-info">
                    <h6>'.$data[$i]['collection'].'</h6>
                    <h4><a href="'.$data[$i]['url'].'" target="_blank">'.$data[$i]['title'].'</a></h4>

                    <div class="price-section">
                        <span>Starting at</span>
                        <span class="price"> $'.$data[$i]['price'].'</span>
                        <span class="sale">On Sale</span>
                    </div>

                    <div class="button-section">
                        <a href="'.$data[$i]['url'].'" class="button" target="_blank">Buy Now</a>
                        <div class="brand-wrap">By '.$data[$i]['brand'].'</div>
                    </div>
                </div>
            </div>
        </li>';
    }
    $result.= '</ul></div>';
    return $result;
}

add_shortcode('area_rug_trading_products', 'arearugProductList');



/* function contact_locationlist($arg)
{
    return '<div class="fl-module fl-module-icon fl-node-5be49d131d770" data-node="5be49d131d770">
            <div class="">
                <div class="fl-icon-wrap">
                    <div id="fl-icon-text-5be49d131d734" class="fl-icon-text">
                        <a href="'.get_option('api_contact_url_sandbox').'" target="_self" class="fl-icon-text-link fl-icon-text-wrap">
                            <p>'.get_option('api_contact_address_sandbox').'</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>';
}
add_shortcode('contactLocation', 'contact_locationlist'); */

function formatPhoneNumber($phoneNumber)
{
    $phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber);

    if (strlen($phoneNumber) > 10) {
        $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
        $areaCode = substr($phoneNumber, -10, 3);
        $nextThree = substr($phoneNumber, -7, 3);
        $lastFour = substr($phoneNumber, -4, 4);

        $phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
    } elseif (strlen($phoneNumber) == 10) {
        $areaCode = substr($phoneNumber, 0, 3);
        $nextThree = substr($phoneNumber, 3, 3);
        $lastFour = substr($phoneNumber, 6, 4);

        $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    } elseif (strlen($phoneNumber) == 7) {
        $nextThree = substr($phoneNumber, 0, 3);
        $lastFour = substr($phoneNumber, 3, 4);

        $phoneNumber = $nextThree.'-'.$lastFour;
    }

    return $phoneNumber;
}

function storelocation_address($arg)
{
    
    $website = json_decode(get_option('website_json'));
    $weekdays = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
    $locations = "<ul class='storename'>";
    $locations .= '<li>';
    
    
    for ($i=0;$i<count($website->locations);$i++) {
        $location_name = isset($website->locations[$i]->name)?$website->locations[$i]->name:"";
        if (in_array(trim($location_name), $arg)){

            $location_address_url =  isset($website->locations[$i]->name)?$website->locations[$i]->name." ":"";
            $location_address_url  .= isset($website->locations[$i]->address)?$website->locations[$i]->address." ":"";
            $location_address_url .= isset($website->locations[$i]->city)?$website->locations[$i]->city." ":"";
            $location_address_url .= isset($website->locations[$i]->state)?$website->locations[$i]->state." ":"";
            $location_address_url .= isset($website->locations[$i]->postalCode)?$website->locations[$i]->postalCode." ":"";
    
            $location_address  = isset($website->locations[$i]->address)?"<p>".$website->locations[$i]->address."</p>":"";
            $location_address .= isset($website->locations[$i]->city)?"<p>".$website->locations[$i]->city.", ":"<p>";
            $location_address .= isset($website->locations[$i]->state)?$website->locations[$i]->state:"";
            $location_address .= isset($website->locations[$i]->postalCode)?" ".$website->locations[$i]->postalCode."</p>":"</p>";
            
            $location_phone  = "";
            if (isset($website->locations[$i]->phone)) {
                $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
            }
            if (isset($website->locations[$i]->forwardingPhone) && $website->locations[$i]->forwardingPhone != "") {
                $forwarding_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->forwardingPhone);
                $forwarding_phone  = formatPhoneNumber($website->locations[$i]->forwardingPhone);
                
            }
            else{
    
                $forwarding_tel ="#";
                $forwarding_phone  = formatPhoneNumber(8888888888);
            }


            if (in_array("loc", $arg)) {

                if (in_array('nolink', $arg)){
                    $locations .= '<div class="store-container">';
                    //$locations .= '<div class="name">'.$location_name.'</div>';
                    $locations .= '<div class="address">'.$location_address.'</div>';
                    //$locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                    $locations .= '</div>';
                }else{

                    $locations .= '<div class="store-container">';
                    //$locations .= '<div class="name">'.$location_name.'</div>';
                    $locations .= '<div class="address"><a href="https://maps.google.com/maps/?q='.urlencode($location_address_url).'" target="_blank" '.'>'.$location_address.'</a></div>';
                    //$locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                    $locations .= '</div>';

                }
                    
                
            }
            if (in_array("forwardingphone", $arg)) {

                    if (in_array('nolink', $arg)){
                        $locations .= "<div class='phone'><span>".$forwarding_phone."</span></div>";
                    }
                    else{
                        $locations .= "<div class='phone'><a href='tel:".$forwarding_tel."'><span>".$forwarding_phone."</span></a></div>";
                    }

            }
            if (in_array("ohrs", $arg)) {
                    $locations .= '<div class="store-opening-hrs-container">';
                    $locations .= '<ul class="store-opening-hrs">';
                        
                    for ($j = 0; $j < count($weekdays); $j++) {
                        $location .= $website->locations[$i]->monday;
                        if (isset($website->locations[$i]->{$weekdays[$j]})) {
                            $locations .= '<li>'.ucfirst($weekdays[$j]).' : <span>'.$website->locations[$i]->{$weekdays[$j]}.'</span></li>';
                        }
                    }
                    $locations .= '</ul>';
                    $locations .= '</div>';
            }
            if (in_array("dir", $arg)) {
                
                    $locations .= '<div class="direction">';
                    $locations .= '<a href="https://maps.google.com/maps/?q='.urlencode($location_address_url).'" target="_blank" class="fl-button" role="button">
                                        <span class="button-text">GET DIRECTIONS</span></a>';
                    $locations .= '</div>';
                
            }
            if (in_array("map", $arg)) {
                
                    $locations .= '<div class="map-container">
                    <iframe src="https://www.google.com/maps/embed/v1/place?key='.GOOGLE_MAP_KEY.'&amp;q='.urlencode($location_address_url).'"&zoom=nn style="width:100%;min-height:450px;border:0px;"></iframe>
                    </div>';
                
            }
            if (in_array("phone", $arg)) {
                        $locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
            }
        } 
        //$website->locations[$i]->address.", ".$website->locations[$i]->city.", ".$website->locations[$i]->state.", ".$website->locations[$i]->postalCode;
       
        
        
        


        /* if (in_array("alldata", $arg)) {
            $locations .= '<div class="store-container">';
            //$locations .= '<div class="name">'.$location_name.'</div>';
            $locations .= '<div class="address"><a href=https://www.google.com/maps/dir//'.urlencode($location_address_url).' target="_blank" '.'>'.$location_address.'</a></div>';
            //  $locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
            $locations .= '</div>';

            $locations .= '<div class="store-opening-hrs-container">';
            $locations .= '<ul class="store-opening-hrs">';
            for ($j = 0; $j < count($weekdays); $j++) {
                $location .= $website->locations[$i]->monday;
                if (isset($website->locations[$i]->{$weekdays[$j]})) {
                    $locations .= '<li>'.ucfirst($weekdays[$j]).' : <span>'.$website->locations[$i]->{$weekdays[$j]}.'</span></li>';
                }
            }
            $locations .= '</ul>';
            $locations .= '</div>';
            $locations .= '<div class="direction">';
            $locations .= '<a href="https://www.google.com/maps/dir//'.urlencode($location_address_url).'" target="_blank" class="fl-button" role="button">
                            <span class="button-text">GET DIRECTIONS</span></a>';
            $locations .= '</div>';
            $locations .= '<div class="map-container">
            <iframe src="https://www.google.com/maps/embed/v1/place?key='.GOOGLE_MAP_KEY.'&amp;q='.urlencode($location_address_url).'"&zoom=nn style="width:100%;min-height:450px;border:0px;"></iframe>
            </div>';
        }  */
        $locations .= '</li>';
    }
    $locations .= '</ul>';
    
    return $locations;
}
    add_shortcode('storelocation_address', 'storelocation_address');

 /*    function storelocation_map($arg)
    {
        $website = json_decode(get_option('website_json'));
        
        for ($i=0;$i<count($website->locations);$i++) {
            $map = '<div class="map-container">
                <iframe src="https://www.google.com/maps/embed/v1/place?key='.GOOGLE_MAP_KEY.'&amp;q='.$website->locations[$i]->address.$website->locations[$i]->city.$website->locations[$i]->postalCode.$website->locations[$i]->state.'"&zoom=nn style="width:100%;min-height:450px;border:0px;"></iframe>
            </div>';
            return $map;
        }
    }
        add_shortcode('storelocation_map', 'storelocation_map');
 */
function contact_phonenumber($atts)
{
    if (get_option('api_contact_phone')) {
        return '<a href="tel:'.get_option('api_contact_phone').'" target="_self" class="fl-icon-text-link fl-icon-text-wrap">'.get_option('api_contact_phone').'</a>';
    } else {
        return '<a href="javascript:void(0)" target="_self" class="fl-icon-text-link fl-icon-text-wrap">(XXX) (XXXXXXX)</a>';
    }
}
add_shortcode('contactPhnumber', 'contact_phonenumber');


/* function contact_address_display($atts)
{
    return '
    <div class="fl-module fl-module-icon fl-node-5be49d131d770" data-node="5be49d131d770">
	<div class="">
    <div class="fl-icon-wrap">

	<span class="fl-icon">
								<a href="'.get_option('api_contact_url_sandbox').'" target="_blank" tabindex="-1" aria-hidden="true" aria-labelledby="fl-icon-text-5be49d131d734">
							<i class="fas fa-map-marker" aria-hidden="true"></i>
				</a>
			</span>

		<div id="fl-icon-text-5be49d131d734" class="fl-icon-text">
				<a href="'.get_option('api_contact_url_sandbox').'" target="_blank" class="fl-icon-text-link fl-icon-text-wrap">
				<p>'.get_option('api_contact_address_sandbox').'</p>				</a>
			</div>
</div>
</div></div>
<div class="fl-module fl-module-icon fl-node-5be49d131d770" data-node="5be49d131d770">
	<div class="">
		<div class="fl-icon-wrap">

	<span class="fl-icon">
								<a href="tel:'.get_option('api_contact_phone').'" target="_blank" tabindex="-1" aria-hidden="true" aria-labelledby="fl-icon-text-5be49d131d770">
							<i class="fas fa-phone" aria-hidden="true"></i>
				</a>
			</span>

		<div id="fl-icon-text-5be49d131d770" class="fl-icon-text">
				<a href="tel:'.get_option('api_contact_phone').'" target="_blank" class="fl-icon-text-link fl-icon-text-wrap">
</a><p><a href="tel:'.get_option('api_contact_phone').'" target="_blank" class="fl-icon-text-link fl-icon-text-wrap"></a><a href="tel:'.get_option('api_contact_phone').'">'.get_option('api_contact_phone').'</a></p>
			</div>
	
</div>
	</div>
</div>'
;
}
add_shortcode('contactAddress', 'contact_address_display');

https://www.google.com/maps/dir//Dalton+Wholesale+Floors,+411+Soho+Dr,+Adairsville,+GA+30103/@34.381066,-84.9075907,17z/data=!4m15!1m6!3m5!1s0x88f555733e5d7859:0x4a06216b9216e888!2sDalton+Wholesale+Floors!8m2!3d34.381066!4d-84.905402!4m7!1m0!1m5!1m1!1s0x88f555733e5d7859:0x4a06216b9216e888!2m2!1d-84.905402!2d34.381066
function getDirectionButton()
{
    return '
		<div class="getdirection">
			<a href="'.get_option('api_contact_url_sandbox').'" target="_blank" class="fl-button" role="button">
							<span class="fl-button-text">GET DIRECTIONS</span>
                    </a>
                    
</div>
	';
}
add_shortcode('getDirection', 'getDirectionButton');

function getOpeningHours()
{
    return '
		<div class="fl-button-wrap fl-button-width-auto fl-button-left">
			'.get_option('api_opening_hr_fields_sandbox').'
                    
</div>
	';
}
add_shortcode('getOpeningHours', 'getOpeningHours'); */

function getSocailIcons()
{
    $details = json_decode(get_option('social_links'));
    $return  = '';
    if (isset($details)) {
        $return  = '<ul class="social-icons">';
        foreach ($details as $key => $value) {
            if ($value->platform=="googleBusiness") {
                $value->platform = "google-plus";
            }
            if ($value->platform=="linkedIn") {
                $value->platform = "linkedin";
            }
                
                
            if ($value->active) {
                $return  .= '<li><a href="'.$value->url.'" target="_blank" title="'.$value->platform.'"><i class="fab fa-'.$value->platform.'"></i></a></li>';
            }
        }
        $return  .= '</ul>';
    }
    return $return;
}
add_shortcode('getSocailIcons', 'getSocailIcons');

function create_post_type()
{
    register_post_type(
        'store-locations',
        array(
        'labels' => array(
          'name' => __('Store Locations'),
          'singular_name' => __('Store Location')
        ),
        'public' => true,
        'has_archive' => true,
      )
    );
}
add_action('init', 'create_post_type');

function contactInformation($atts)
{
    $contacts = json_decode(get_option('website_json'));
    $info="";
    if (is_array($contacts->contacts)) {
        if (in_array("withicon", $atts)) {
            for ($i=0;$i<count($contacts->contacts);$i++) {
                if (in_array($contacts->contacts[$i]->type, $atts)) {
                    if (in_array("email", $atts)) {
                        $info  .= "<a class='mail-icon icon-link' href='mailto:".$contacts->contacts[$i]->email."'>".$contacts->contacts[$i]->email."</a>";
                    }
                    if (in_array("phone", $atts)) {
                        $info  .= "<a class='phone-icon icon-link' href='tel:".preg_replace('/[^0-9]/', '', $contacts->contacts[$i]->phone)."'>".formatPhoneNumber($contacts->contacts[$i]->phone)."</a>";
                    }
                    if (in_array("notes", $atts)) {
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        } else {
            for ($i=0;$i<count($contacts->contacts);$i++) {
                if (in_array($contacts->contacts[$i]->type, $atts)) {
                    if (in_array("email", $atts)) {
                        $info  .= "<a class='mail-icon-with icon-link-with' href='mailto:".$contacts->contacts[$i]->email."'>".$contacts->contacts[$i]->email."</a>";
                    }
                    if (in_array("phone", $atts)) {
                        $info  .= "<a class='phone-icon-with icon-link-with' href='tel:".preg_replace('/[^0-9]/', '', $contacts->contacts[$i]->phone)."'>".formatPhoneNumber($contacts->contacts[$i]->phone)."</a>";
                    }
                    if (in_array("notes", $atts)) {
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        }
    }
    return $info;
}
add_shortcode('contactInformation', 'contactInformation');


function locationInformation($atts)
{
    $website = json_decode(get_option('website_json'));
    
    for ($i=0;$i<count($website->locations);$i++) {
        if (in_array($website->locations[$i]->name, $atts)) {
            if (in_array("license", $atts)) {
                $info  .= "<div class='store-license'>".$website->locations[$i]->licenseNumber."</div>";
            }
            if (in_array("phone", $atts)) {
                $info  .= "<a class='phone-icon-with icon-link-with' href='tel:".preg_replace('/[^0-9]/', '', $website->locations[$i]->phone)."'>".formatPhoneNumber($website->locations[$i]->phone)."</a>";
            }
        }
    }
    return $info;
}
add_shortcode('locationInformation', 'locationInformation');


function hookSharpStrings() {

    $website_json =  json_decode(get_option('website_json'));
    $sharpspringDomainId =$sharpspringTrackingId ="";
    for($j=0;$j<count($website_json->sites);$j++){
        
        if($website_json->sites[$j]->instance == ENV){
           
                $sharpspringTrackingId = $website_json->sites[$j]->sharpspringTrackingId;
                $sharpspringDomainId = $website_json->sites[$j]->sharpspringDomainId;
           
            }
        }
        
       if(isset($sharpspringTrackingId) && trim($sharpspringTrackingId) !="" ){
   
    ?>
           <script type="text/javascript">

                    var _ss = _ss || [];

                    _ss.push(['_setDomain', 'https://<?php echo $sharpspringDomainId;?>.marketingautomation.services/net']);

                    _ss.push(['_setAccount', '<?php echo $sharpspringTrackingId;?>']);

                    _ss.push(['_trackPageView']);

                    (function() {

                    var ss = document.createElement('script');

                    ss.type = 'text/javascript'; ss.async = true;

                    ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + '<?php echo $sharpspringDomainId;?>.marketingautomation.services/client/ss.js?ver=1.1.1';

                    var scr = document.getElementsByTagName('script')[0];

                    scr.parentNode.insertBefore(ss, scr);

                    })();

                    </script>
    <?php
       }
}
add_action('wp_head', 'hookSharpStrings');

function getRetailerInfo($arg){


    $website = json_decode(get_option('website_json'));
    if (in_array('city', $arg)){
        $info =  isset($website->locations[0]->city)?"<span class='city retailer'>".$website->locations[0]->city."</span>":"";
    }
    else if (in_array('state', $arg)){
        $info =  isset($website->locations[0]->state)?"<span class='state retailer'>".$website->locations[0]->state."</span>":"";
    }
    else if (in_array('zipcode', $arg)){
        $info =  isset($website->locations[0]->postalCode)?"<span class='zipcode retailer'>".$website->locations[0]->postalCode."</span>":"";
    }
    else if (in_array('legalname', $arg)){
        $info =  isset($website->name)?"<span class='name retailer'>".$website->name."</span>":"";
    }
    else if (in_array('address', $arg)){
        $info =  isset($website->locations[0]->address)?"<span class='street_address retailer'>".$website->locations[0]->address."</span>":"";
    }
    else if (in_array('phone', $arg)){
        if (isset($website->locations[0]->phone)) {
            $location_tel = preg_replace('/[^0-9]/', '', $website->locations[0]->phone);
            $location_phone  = formatPhoneNumber($website->locations[0]->phone);
        }
        if (in_array('nolink', $arg)){
            $info =  isset($website->locations[0]->phone)?"<span class='phone retailer'>".$location_phone."</span>":""; 
        }
        else{
            
            $info =  isset($website->locations[0]->phone)?"<a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a>":"";
        }
    }
    else if (in_array('forwarding_phone', $arg)){
        if (isset($website->locations[0]->forwardingPhone)) {
            $location_tel = preg_replace('/[^0-9]/', '', $website->locations[0]->forwardingPhone);
            $location_phone  = formatPhoneNumber($website->locations[0]->forwardingPhone);
        }
        if (in_array('nolink', $arg)){
            $info =  isset($website->locations[0]->forwardingPhone)?"<span class='phone retailer'>".$location_phone."</span>":""; 
        }
        else{
            
            $info =  isset($website->locations[0]->forwardingPhone)?"<a href='tel:".$location_tel."' class='phone retailer' >"."<span>".$location_phone."</span></a>":"";
        }
    }
    else if (in_array('companyname', $arg)){
        for($j=0;$j<count($website->sites);$j++){
        
            if($website->sites[$j]->instance == ENV){
                    $info =  isset($website->sites[$j]->name)?"<span class='companyname retailer'>".$website->sites[$j]->name."</span>":"";
                    break;
                }
            }
    }
    else if (in_array('site_url', $arg)){
        for($j=0;$j<count($website->sites);$j++){
        
            if($website->sites[$j]->instance == ENV){
                    $info =  isset($website->sites[$j]->url)?"<span class='site_url retailer'>".$website->sites[$j]->url."</span>":"";
                    break;
                }
            }
    }
    
    return $info;  
}

add_shortcode('Retailer', 'getRetailerInfo');

function copyrightstext($arg)
{
    $result = "<p>Copyright ©".do_shortcode('[fl_year]')." ".get_bloginfo()." All Rights Reserved.</p>";
    return $result;
}

add_shortcode('copyrights', 'copyrightstext');